# star-edge-cloud

#### 介绍
star-edge-cloud是一个经过边缘端数据采集分析后汇总到云端进行大数据处理的物联网边缘计算-云计算平台。目标是帮助一些监控类项目提供一个可靠的、安全的，简单易用解决方案。

QQ交流群： 590749338

#### 软件架构
软件架构说明


## 部署教程
在Linux--Deepin15.5下,进入deploy目录，执行编译脚本。

### edge端

1. 执行部署命令：
```
cd deploy/
sudo chmod +x edge.sh
./edge.sh
```
2. 运行系统：
```
sudo ./core
```
3. 访问：[http://localhost:21000/html/index.html](http://localhost:21000/html/index.html)
4. 运行log服务
5. 运行store服务
6. 添加设备设备，选择compile目录下编译好的文件
7. 添加扩展设备，选择compile目录下编译好的文件

### cloud端

测试坏境搭建（安装docker,docker-compose略)：

1. 打开2375端口（后面将使用tls访问，开启2376）：
```
vi /lib/systemd/system/docker.service
```
2. 找到Execstart=/usr/bin/dockerd后加上
```
-H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock  
```
保存并且退出

```
systemctl daemon-reload
service docker restart//重启启动docker
systemctl stats docker//可以查看相关内容，看看2375是否已经设置好
```

3. 访问和验证：
[http://localhost:2375/info](http://localhost:2375/info)

4. 拉取hbase容器 
```
docker pull harisekhon/hbase 
```
5. 启动容器 
```
docker run -d -h myhbase -p 2181:2181 -p 8080:8080 -p 8085:8085 -p 9090:9090 -p 9095:9095 -p 16000:16000 -p 16010:16010 -p 16201:16201 -p 16301:16301 -p 16020:16020 -p 16030:16030 --name hbase1.3 harisekhon/hbase
```

6. 访问及验证hbase
[http://localhost:16010/master-status](http://localhost:16010/master-status)

7. 执行命令：
```
cd deploy/
sudo chmod +x cloud.sh
mvn clean package
./cloud.sh
```
8. 运行：
```
java -jar caas*.jar
#这种方法还没有尝试:nohup java -jar ***.jar &
```
9. 部署tomcat
下载：[http://mirrors.shu.edu.cn/apache/tomcat/tomcat-8/v8.5.37/bin/apache-tomcat-8.5.37.tar.gz](http://mirrors.shu.edu.cn/apache/tomcat/tomcat-8/v8.5.37/bin/apache-tomcat-8.5.37.tar.gz)
10. 进入目录后运行：
```
./startup.sh
```
11. 拷贝display下web项目到webapps之中
```
cp -r */web */webapps/
```
12. 访问及验证：
[http://localhost:8080/web/index.html](http://localhost:8080/web/index.html)

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)