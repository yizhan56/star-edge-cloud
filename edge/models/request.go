package models

// Request -
type Request struct {
	ID      string
	Status  string
	Message []byte
}
